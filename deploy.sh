TOKEN=$1
ID=$(docker inspect registry.heroku.com/mad-web/web --format="{{.ID}}")
curl --output /dev/mull -X PATCH https://api.heroku.com/apps/mad-web/formation \
-d '{
  "updates": [
    {
      "type": "web",
      "docker_image": "'"$ID"'"
    }
  ]
}' \
-H "Content-Type: application/json" \
-H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
-H "Authorization: Bearer $TOKEN"