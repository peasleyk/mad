let wsuri;
let broke = false;

// Initialize Sock and add callback functions
if (window.location.hostname == "localhost") {
  wsuri = "ws://" + window.location.host + "/ws";
} else {
  wsuri = "wss://" + window.location.host + "/ws";
}
const sock = new WebSocket(wsuri);

sock.onopen = () => {
  console.log("connected to " + wsuri);
};

sock.onerror = function(event) {
  console.error("WebSocket error observed:", event);
};

sock.onclose = () => {
  broke = true;
  console.log("leaving...");
};

sock.onmessage = function (payload) {
  update("Finished generating");
  document.getElementById("submitButton").disabled = false;
  document.getElementById("output").value = payload.data;
};

function update(message) {
  document.getElementById("status").innerHTML = message;
}

function send() {
  if (broke) {
    update("Connection broke - refresh page");
    document.getElementById("submitButton").disabled = true;
    return;
  }

  const body = document.getElementById("input").value;
  const lengthType = document.getElementById("lengthTypeBox").value;
  const length = parseInt(document.getElementById("legnthInput").value);
  const nGramSize = parseInt(document.getElementById("ngramInput").value);
  const splitType = document.getElementById("splitTypeBox").value;
  const Inputs = {
    Body: body,
    Type: lengthType,
    Length: length,
    GramSize: nGramSize,
    Split: splitType,
  };
  const reply = JSON.stringify(Inputs);
  document.getElementById("submitButton").disabled = true;
  update("Generating chain....");
  sock.send(reply);
}

showLines();
function showLines() {
  const text = document.getElementById("input").value;
  // If too memory intensive use regex, this is fast but bad
  lines = text.split("\n").length - 1;
  chars = text.length
  document.getElementById("text_lines").innerHTML = `Lines: ${lines}`;
  document.getElementById("text_length").innerHTML = `Length: ${chars}`;

}
