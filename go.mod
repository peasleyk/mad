module gitlab.com/peasleyk/mad

go 1.13

require (
	github.com/magefile/mage v1.11.0
	gitlab.com/peasleyk/gomark v1.2.0
	nhooyr.io/websocket v1.8.4
)
