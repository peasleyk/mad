//+build mage

// Mage build script for My blog. See options below
package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

// Globals used in jobs below
var BINARY_NAME = "mad"
var BUILD_DIR = "./bin"
var BINARY_PATH = filepath.Join(BUILD_DIR, BINARY_NAME)
var BUILD_PARAMS = "-ldflags=-s -w"
var Default = Dev
var PROJECT_ID = "7679953"
var JOB_LIMIT = 5

// Response of gitlab jobs
type Job struct {
	Ref        string `json:"ref"`
	Created_at string `json:"created_at"`
	Name       string `json:"name"`
	Status     string `json:"status"`
}

// Builds the binary.
func Build() error {
	gocmd := mg.GoCmd()
	return sh.RunV(gocmd, "build", "-o", BINARY_PATH)
}

// Builds a release version with more checks
func BuildR() error {
	mg.Deps(Clean, Check)
	gocmd := mg.GoCmd()
	return sh.RunV(gocmd, "build", BUILD_PARAMS, "-o", BINARY_PATH)
}

// Starts the development server
// This opens a firefox page to the website, as well as reloads the posts
// on file change
func Dev() (err error) {
	mg.Deps(Build)
	port := port()
	// gocmd := mg.GoCmd()
	if err := sh.RunV("firefox", fmt.Sprintf("localhost:%s", port)); err != nil {
		return err
	}
	if err := sh.RunV(BINARY_PATH, "--port", port); err != nil {
		return err
	}
	return nil
}

// Run the server in production mode
func Run() error {
	mg.Deps(BuildR)
	port := port()
	envs := map[string]string{"PORT": port}
	if err := sh.RunWith(envs, BINARY_PATH, "--port", port); err != nil {
		return err
	}
	return nil
}

// Remove the temporarily generated files from Build.
func Clean() error {
	cmd := mg.GoCmd()
	err := sh.RunV(cmd, "clean")
	err = sh.Rm(BUILD_DIR)
	if err != nil {
		return err
	}
	return nil
}

// Builds and runs a docker container
func Docker() error {
	port := port()
	if err := sh.RunV("docker", "build", "--force-rm", "-t", BINARY_NAME+":latest", "."); err != nil {
		return err
	}
	if err := sh.RunV("docker", "run", "--rm", "--init", "-p", fmt.Sprintf("%s:%s", port, port), BINARY_NAME+":latest"); err != nil {
		return err
	}
	return nil

}

// Go tools to run before committing
// This sticks to built in tooling for now
func Check() {
	cmd := mg.GoCmd()
	// Neat way to kick off an anonymous function
	defer func() {
		{
			sh.RunV(cmd, "fmt")
			sh.RunV(cmd, "vet")
			sh.RunV(cmd, "mod", "tidy")
			sh.RunV(cmd, "mod", "verify")
			sh.RunV("golangci-lint", "run")
			sh.RunV(cmd, "clean")
		}
	}()
}

// Determines what port to use
func port() string {
	p := os.Getenv("PORT")
	if p == "" {
		return "12345"
	}
	return p
}

// Checks status of gitlab jobs
func Status() {
	jobs, err := getJobs()
	if err != nil {
		fmt.Printf("Erro gettings jobs: %v", err.Error())
		return
	}
	res := maxes(jobs)

	len_total := 0
	for _, v := range res {
		len_total += v
	}

	println("+---" + strings.Repeat("-", len_total+len(res)) + "----+")
	for x := 0; x < JOB_LIMIT; x++ {
		e := reflect.ValueOf(&jobs[x]).Elem()
		out := ""
		for i := 0; i < e.NumField(); i++ {
			name := e.Type().Field(i).Name
			value := e.Field(i).String()
			if i == e.NumField()-1 {
				out += fmt.Sprintf("| %-*s |", res[name], value)
			} else {
				out += fmt.Sprintf("| %-*s ", res[name], value)
			}
		}
		println(out)
	}
	println("+---" + strings.Repeat("-", len_total+len(res)) + "----+")
}

// Dynamically gets all max lengths of struct fields
// A little hacky but I'm a little lazy
func maxes(j []Job) map[string]int {
	m := make(map[string]int)
	if len(j) < 1 {
		return nil
	}

	for x := range j {
		// reflect.Value
		e := reflect.ValueOf(&j[x]).Elem()
		for i := 0; i < e.NumField(); i++ {
			// Type contains a structField
			name := e.Type().Field(i).Name
			// still in reflect.value
			value := e.Field(i).String()
			if m[name] == 0 {
				m[name] = len(value)
				continue
			}
			if len(value) > m[name] {
				m[name] = len(value)
			}
		}
	}

	return m
}

func getJobs() ([]Job, error) {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/jobs", PROJECT_ID), nil)
	req.Header.Set("PRIVATE-TOKEN", os.Getenv("GKEY"))
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var jobs []Job
	json.NewDecoder(resp.Body).Decode(&jobs)

	if len(jobs) < 1 {
		return nil, errors.New("No Jobs found")
	}
	return jobs, nil
}
