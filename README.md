# Mad
A web front end for my markov chain library https://gitlab.com/peasleyk/gomark

See it in action: https://mad.peasley.me - clients are connected by websocket

## Running

Mage is used as a build/run script

```shell
» go get
» mage buildr
» mage run
2020/09/12 19:01:32 Server running on 12345
```

Or the old fashioned way,
```shell
» go build
» ./mad
```