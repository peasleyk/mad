package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"html/template"
	"log"
	"math"
	"net/http"
	"strings"
	"time"

	chain "gitlab.com/peasleyk/gomark"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

// Page contains website information to template in
type Page struct {
	Title string
	Input string
}

// Inputs contains the web socket request
type Inputs struct {
	Body     string
	Type     string
	Length   int
	GramSize int
	Split    string
}

// Handles incoming ws text
func wsHandler(w http.ResponseWriter, r *http.Request) {
	wc, err := websocket.Accept(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer wc.Close(websocket.StatusInternalError, "Closing websocket...")
	// Not too large to freeze our VPS... or our transfer limits
	wc.SetReadLimit(int64(math.Pow(float64(2), float64(20))))

	// No timeout, because we want this connection to stay open
	ctx, cancel := context.WithCancel(r.Context())
	defer cancel()

	// Keeps our connection open
	// Need to catch here for close?
	for {
		req, err := getRequest(ctx, wc)
		if err != nil {
			// Check length error
			sendMessage(ctx, wc, err.Error())
			if websocket.CloseStatus(err) != websocket.StatusNormalClosure {
				log.Print("Client disconnected, closing connection")
				wc.Close(websocket.StatusGoingAway, "Client closed")
				break // cancels our connection
			}
		}
		err = req.validate()
		if err != nil {
			err = fmt.Errorf("Error getting input: %v", err)
			sendMessage(ctx, wc, err.Error())
			continue
		}

		msg, err := req.generateChain()
		if err != nil {
			sendMessage(ctx, wc, err.Error())
			continue
		}

		sendMessage(ctx, wc, msg)
	}
}

// Given an incoming message, decide what to send
func getRequest(ctx context.Context, c *websocket.Conn) (Inputs, error) {
	// Transform the incoming json to a readable interface
	var i Inputs
	err := wsjson.Read(ctx, c, &i)
	if err != nil {
		return i, err
	}
	return i, nil
}

// Push a message back to the client
// We really don't care if it fails, so just log it here
func sendMessage(ctx context.Context, c *websocket.Conn, msg string) {
	b := []byte(msg)
	if err := c.Write(ctx, websocket.MessageText, b); err != nil {
		log.Printf("Error sending message to client: %v", err)
	}
}

// Generates text given the input using gomark
func (i *Inputs) generateChain() (string, error) {
	gmap, err := chain.NewGramMap(i.GramSize, i.Split)
	if err != nil {
		return "", fmt.Errorf("Can't initialize Markov chain: %v", err)
	}

	err = gmap.BuildMapFromText(strings.NewReader(i.Body))
	if err != nil {
		return "", fmt.Errorf("Can't build Markov map for some reason: %v", err)
	}

	var msg string
	switch i.Type {
	case "word":
		msg, err = gmap.BuildTextWordLimit(i.Length, 100)
	case "character":
		msg, err = gmap.BuildTextCharLimit(i.Length, 100)
	}
	if err != nil {
		return "", fmt.Errorf("Can't build an output, try a longer input or different settings: %v", err)
	}

	return msg, nil
}

// Double checking input
func (i *Inputs) validate() error {
	if len(i.Body) == 0 {
		return errors.New("Input is empty")
	}
	if i.Type != "word" && i.Type != "char" {
		return errors.New("Length type must be character or word")
	}
	if i.Length < 100 || i.Length > 5000 {
		return errors.New("Length limit must be 100-5000")
	}
	if i.GramSize < 1 || i.GramSize > 10 {
		return errors.New("N Gram size must be 1-10")
	}
	if i.Split != "word" && i.Split != "char" {
		return errors.New("Split type must be character or word")
	}
	return nil
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "404", http.StatusInternalServerError)
		return
	}
	if r.Method != "GET" {
		//nolint
		w.Write([]byte("Uhhhhh\n"))
		return
	}
	tmpl := template.Must(template.ParseFiles("static/templates/index.html"))

	data := Page{
		Title: "Markov Text Generator",
		Input: "Enter Text",
	}
	err := tmpl.Execute(w, data)
	if err != nil {
		panic(err.Error())
	}
}

func main() {
	port := flag.String("port", "12345", "Port to run on")
	flag.Parse()

	serverMux := http.NewServeMux()
	serverMux.HandleFunc("/ws", wsHandler)

	// Send our JS and css
	serverMux.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))
	serverMux.HandleFunc("/", rootHandler)
	srv := &http.Server{
		Addr:         ":" + *port,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
		Handler:      serverMux,
	}
	log.Printf("Server running on %s", *port)
	log.Fatal(srv.ListenAndServe())
}
