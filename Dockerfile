FROM golang:1.14.7-stretch AS builder

COPY . /apps/mad
WORKDIR /apps/mad

# CGO enabled so we can use the static ld flags
# This lets us build a static binary, as our final container
# will have nothing to link to
RUN GOBIN=$GOPATH/bin go get && \
    CGO_ENABLED=0 go build -ldflags '-extldflags "-static"' -o mad && \
    addgroup --system  nonroot && \
    useradd -g nonroot nonroot

# Copy over our built binary as well as our non root user
FROM scratch
COPY --from=builder /apps/mad/ /apps/mad/
COPY --from=builder /etc/passwd /etc/passwd

WORKDIR /apps/mad

EXPOSE 5000
CMD ["./mad", "-port", "5000"]